(ns icalgram.frontend
  (:require
   [reagent.dom :as rdom]
   [icalgram.router :as rt :refer [routes-state]]
   [reitit.frontend.easy :as rfe]
   [icalgram.page.event-form :refer [event-form-state]]
   [icalgram.hooks :as hook]))

(defn current-page []
  [:div
   (let [view (-> @routes-state :data :view)]
     [:div  {:style {:display "flex"
                     :flex-direction "column"
                     :align-items "center"
                     :justify-content "space-between"}}
      [:nav
       [:menu
        [:li [:a {:href (rfe/href :router/create-event)} "Создать"]]
        [:li [:a {:href (rfe/href :router/list-events)} "Список"]]]]
      [view]])])

(defn mini-app []
  (hook/create-main-button event-form-state))

(defn init []
  (rt/router)
  (mini-app)
  (rdom/render [current-page] (js/document.getElementById "root")))
