(ns icalgram.router
  (:require
   [icalgram.page.event-form :as form]
   [icalgram.page.user-events :as user]
   [reagent.core :as r]
   [reitit.coercion.spec :as rss]
   [reitit.frontend :as rf]
   [reitit.frontend.easy :as rfe]))

(defonce routes-state (r/atom nil))

(def routes
  [["/" {:name :router/create-event
         :view #'form/event-form}]
   ["/my-events" {:name :router/list-events
                  :view #'user/list-events}]
   ["/event/:id" {:name :router/event
                  :view #'user/event-form-edit}]])

(defn router []
  (rfe/start!
   (rf/router routes {:data {:coercion rss/coercion}})
   (fn [matched-router] (reset! routes-state matched-router))
   {:use-fragment false}))
