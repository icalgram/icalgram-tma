(ns icalgram.hooks
  (:require
   ["@tma.js/sdk" :as tma :refer [retrieveLaunchParams]]
   [icalgram.ajax :as req]))

(def init-data
  (let [init (try
               (js->clj (retrieveLaunchParams tma/InitData) :keywordize-keys true)
               (catch js/Error e (prn e)))]
    {:user-id (-> init :initData :user :id)}))

(def theme-params
  (let [theme js/Telegram.WebApp.themeParams]
    {:button-color (.-button_color theme)
     :hint-color (.-hint_color theme)}))

(defn main-button-hook [bool]
  (let [mb js/Telegram.WebApp.MainButton]
    (if (true? bool)
      (do (.enable mb ) (.setParams mb (clj->js {:color (:button-color theme-params)})))
      (do (.disable mb) (.setParams mb (clj->js {:color (:hint-color theme-params)}))))))

(defn update-state [m & opts]
  (apply swap! m opts)
  (let [event-fields [:summary :start-date :end-date]]
    (-> (every? (comp seq str) (map #(get @m %) event-fields))
        main-button-hook))
  (prn @m))

;; (defn pop-up-delete []
;;   (js/Telegram.WebApp.showPopup (clj->js {:title "Удалить Событие"
;;                                           :message "Событие будет удалено"
;;                                           :buttons [{:type "destructive" :text "Удалить"}
;;                                                     {:type "cancel"
;;                                                      :id "2"}]})))

(defn create-main-button [state]
  (let [mb js/Telegram.WebApp.MainButton]
    (.setParams mb (clj->js {:text "Сохранить"
                             :is_active false
                             :is_visible false
                             :color (:hint-color theme-params)}))
    (.onClick mb #(do (req/save-event state (:user-id init-data))
                      (.showProgress mb)))))

(defn open-main-buttun []
  (let [mb js/Telegram.WebApp.MainButton]
    (.show mb)))

(defn close-main-button []
  (let [mb js/Telegram.WebApp.MainButton]
    (.hideProgress mb)
    (.disable mb)
    (.hide mb)))
