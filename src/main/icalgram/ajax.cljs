(ns icalgram.ajax
  (:require
   [ajax.core :refer [GET POST DELETE]]
   [reitit.frontend.easy :as rfe]))

(def backend-api "BACKEND_URL")

(defn format-url [& path]
  (apply str (interpose "/" path)))

(defn event-handler [response]
  (rfe/push-state :router/list-events))

(defn save-event [state user-id]
  (swap! state assoc :user-id user-id)
  (POST (format-url backend-api "newEvent")
        {:format :json
         :params @state
         :handler event-handler}))

(defn get-events [user-id state]
  (GET (format-url backend-api "events" user-id)
       {:keywords? true
        :response-format :json
        :handler (fn [req] (reset! state req))}))

(defn delete-event [event-uuid]
  (DELETE (format-url backend-api "event" event-uuid)
          {:handler event-handler}))

