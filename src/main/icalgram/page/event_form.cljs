(ns icalgram.page.event-form
  (:require
   [reagent.core :as r]
   ["flatpickr" :as flatpickr]
   [icalgram.hooks :as hook]
   [icalgram.ajax :as req]))

(defonce event-form-state (r/atom nil))

(defn event-input [opts]
  (let [id (-> opts :id keyword)]
    [:input.event-input
     (merge opts
            {:on-change #(hook/update-state event-form-state assoc id (-> % .-target .-value))
             :value (id @event-form-state)})]))

(defn datetime-picker [props]
  (let [flatpickr-instance (r/atom nil)
        date-picker-ref (r/atom nil)
        id (-> props :id keyword)]

    (r/create-class
      {:component-did-mount
       (fn []
         (when-let [el @date-picker-ref]
           (let [on-change (fn [selectedDates dateStr instance]
                             (hook/update-state event-form-state assoc id (-> selectedDates)))
                 instance (flatpickr el (clj->js {:static true
                                                  :closeOnSelect false
                                                  :minDate "today"
                                                  :enableTime true :time_24hr true
                                                  :onChange on-change}))]
             (reset! flatpickr-instance instance))))

       :component-will-unmount
       (fn []
         (when-let [instance @flatpickr-instance]
           (.destroy instance)))

       :reagent-render
       (fn []
         [:div ;{:style {:display "flex" :flex-shrink 0}}
          [:input.event-input (merge props {:ref #(reset! date-picker-ref %)
                                            :type "text"
                                            :defaultValue (id @event-form-state)})]])})))

(defn event-fields []
  [:div.event-fields
   [:label "Наименование"
    [event-input {:id "summary" :type "text" :placeholder "Обсуждение"}]]
   [:label "Время и дата"
    [datetime-picker {:id "start-date" :placeholder "начало"}]
    [datetime-picker {:id "end-date" :placeholder "конец"}]]
   [:label "Описание"
    [event-input {:id "description" :type "text" :placeholder "Встреча в google meets"}]]])


(defn event-form
  ([]
   (reset! event-form-state nil)
   (event-form "Создать Событие"))
  ([title & attrs]
   (hook/open-main-buttun)
   [:div {:style {:display "flex"
                  :flex-direction "column"
                  :align-items "center"
                  :justify-content "space-between"}}
    [:h2 title]
    [:form
     [event-fields]]
    (into [:div] attrs)]))

(defn fill-user-event-form [event]
  (reset! event-form-state event)
  (prn @event-form-state))

