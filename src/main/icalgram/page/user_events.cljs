(ns icalgram.page.user-events
  (:require
   [reagent.core :as r]
   [icalgram.page.event-form :as form :refer [event-form-state]]
   [icalgram.ajax :as req]
   [icalgram.hooks :as hook]
   [reitit.frontend.easy :as rfe]))

(def parser-props
  {:month-day #js {:month "long" :day "numeric"}
   :time #js {:hour "numeric"
              :minute "numeric"}
   :minutes #js {:hour "numeric"
                 :minute "numeric"}})

(defonce user-events (r/atom nil))

(defn date-string-parser [date-string props]
  (let [date (js/Date. date-string)]
    (-> (js/Intl.DateTimeFormat "ru-RU" props)
        (.format date))))

(defn format-duration [total-ms]
  (let [hours (Math/floor (/ (/ total-ms 1000) 60 60))
        minutes (mod (/ (/ total-ms 1000) 60) 60)]
    (if (zero? hours)
      (str minutes " мин")
      (str hours " час, " minutes " мин"))))

;; TODO
;; возможность изменить или удалить событие
;; открывать по клику на запись таблицы
;; redict to my-events after deletion
(defn to-row [coll]
  (let [{:keys [summary start-date end-date event-uuid] :as event} coll
        start (date-string-parser start-date (:time parser-props))
        ;; end (date-string-parser end-date (:time parser-props))
        start-dt (js/Date. start-date)
        end-dt (js/Date. end-date)
        event-parsed (assoc event :start-date [start-dt] :end-date [end-dt])
        duration (- end-dt start-dt)]
    [:tr
     [:td [:a {:href (rfe/href :router/event {:id event-uuid})
               :on-click #(form/fill-user-event-form event-parsed)} summary]]
     [:td start]
     [:td (format-duration duration)]]))

(defn to-table [colls]
  (->> colls
       (group-by (comp #(date-string-parser % (:month-day parser-props)) :start-date))
       (mapcat (fn [[day events]]
                 (cons [:tr [:th {:colSpan 3} day]]
                       (map to-row events))))
       (into [:tbody])))

(defn list-events []
  (hook/close-main-button)
  (req/get-events (:user-id hook/init-data) user-events)
  (fn []
    [:div {:style {:display "flex"
             :flex-direction "column"
             :align-items "center"
             :justify-content "space-between"}}
     [:h2 "Список Событий"]
     (if @user-events
       [:table
        [:thead
         [:tr
          [:th "Название"]
          [:th "Время начала"]
          [:th "Длительность"]]]
        [to-table @user-events]]
       [:p "Загрузка..."])]))


(defn delete-from-state [state {:keys [event-uuid]}]
  (swap! state
         #(filter (fn [m] (not= event-uuid (get m :event-uuid))) %)))

;; (defn add-to-state [state event]
;;   (swap! state conj event))

;; (defn main-button []
;;   (let [mb js/Telegram.WebApp.MainButton]
;;     (.setParams mb (clj->js {:text "Создать"
;;                              :is_active false
;;                              :is_visible true
;;                              :color (:hint-color hook/theme-params)}))
;;     (.onClick mb #(do
;;                     (req/save-event event-form-state (:user-id hook/init-data))
;;                     (.showProgress mb)
;;                     (.hide mb)
;;                     (add-to-state user-events @event-form-state)
;;                     (rfe/push-state :router/list-events)))))

(defn event-form-edit []
  [form/event-form
   "Изменить Событие"
   [:button
    {:type "reset"
     :on-click #(do
                  (req/delete-event (:event-uuid @event-form-state))
                  (delete-from-state user-events (select-keys @event-form-state [:event-uuid])))}
    "Удалить"]])

;; (req/get-events 454517202 user-events)

;; (req/save-event event-form-state 454517202)
